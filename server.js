const express = require('express')
const next = require('next')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev, dir: 'src' })
const handle = app.getRequestHandler()

app
  .prepare()
  .then(() => {
    const server = express()

    server.get('/wp-json/menus/v1/menus/header-menu', (req, res) => {
      res.json([])
    })
    server.get('/wp-json/postlight/v1/frontpage', (req, res) => {
      res.json({
        title: {
          rendered: 'Prova',
        },
        content: {
          rendered: '<h1>Prova</h1><p>questa è una prova...</p>',
        }
      });
    })
    server.get('/wp-json/wp/v2/posts', (req, res) => {
      res.json([])
    })
    server.get('/wp-json/wp/v2/pages', (req, res) => {
      res.json([])
    })

    server.get('/post/:slug', (req, res) => {
      const actualPage = '/post'
      const queryParams = { slug: req.params.slug, apiRoute: 'post' }
      app.render(req, res, actualPage, queryParams)
    })

    server.get('/page/:slug', (req, res) => {
      const actualPage = '/post'
      const queryParams = { slug: req.params.slug, apiRoute: 'page' }
      app.render(req, res, actualPage, queryParams)
    })

    server.get('/category/:slug', (req, res) => {
      const actualPage = '/category'
      const queryParams = { slug: req.params.slug }
      app.render(req, res, actualPage, queryParams)
    })

    server.get('/_preview/:id/:wpnonce', (req, res) => {
      const actualPage = '/preview'
      const queryParams = { id: req.params.id, wpnonce: req.params.wpnonce }
      app.render(req, res, actualPage, queryParams)
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(3000, err => {
      if (err) throw err
      console.log('> Ready on http://localhost:3000')
    })
  })
  .catch(ex => {
    console.error(ex.stack)
    process.exit(1)
  })
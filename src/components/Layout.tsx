import React from 'react'
//import Link from 'next/link'
import { Layout } from 'antd';

const { Header, Footer, Content } = Layout

export default () => (
  <Layout>
    <Header>Header</Header>
    <Content>Content</Content>
    <Footer>Footer</Footer>
  </Layout>
)
